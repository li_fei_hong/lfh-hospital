package com.lfh.lfhpaystarter.config;


import com.lfh.lfhpaystarter.service.AlibabapayQrCodeService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(value = {PayConfig.class})//使使用 @ConfigurationProperties 注解的类生效。
@ConditionalOnClass(AlibabapayQrCodeService.class)//是Springboot实现自动配置的重要支撑之一。其用途是判断当前classpath下是否存在指定类，若是则将当前的配置装载入spring容器
public class PayAutoPropertiesConfig {


    @Bean
    @ConditionalOnProperty(prefix="lfh.lfhpaystarter",name = "isPay",havingValue = "true")
    @ConditionalOnMissingBean
    public AlibabapayQrCodeService alibabapayQrCodeService(){
        return new AlibabapayQrCodeService();
    }


}
