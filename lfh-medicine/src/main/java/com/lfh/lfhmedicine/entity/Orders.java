package com.lfh.lfhmedicine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @ProjectName: lfh-hospital
 * @Package: com.lfh.lfhmedicine.entity
 * @ClassName: Orders
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/16 9:32
 * @Version: 1.0
 */
@Data
public class Orders {
    @TableId(value = "id", type = IdType.AUTO)
    private  Integer id;
    private Integer userid;//用户id
    private Integer orderid;//订单id
    private String name;//药品名称
    private Double sums;//单次用量
    private String yongfa;//用法
    private String pindu;//频度
    private Integer tianshu;//天数
    private Integer number;//数量
    private String numbertype;//数量类型
    private Double price;//单价

}
