package com.lfh.lfhmedicine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ProjectName: lfh-hospital
 * @Package: com.lfh.lfhmedicine.entity
 * @ClassName: user
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/16 9:20
 * @Version: 1.0
 */
@Data
@TableName(value = "user")
public class User {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private  String name;
    private String number;
    private Integer age;
    private Integer sex;
    private String phone;
    private String identity;
    private String url;
    private Date userdate;

}
