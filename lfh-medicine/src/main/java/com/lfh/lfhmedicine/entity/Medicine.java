package com.lfh.lfhmedicine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @ProjectName: lfh-hospital
 * @Package: com.lfh.lfhmedicine.entity
 * @ClassName: Medicine
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/16 11:52
 * @Version: 1.0
 */
@Data
public class Medicine {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private String type;
    private Integer number;
    private Double price;
}
