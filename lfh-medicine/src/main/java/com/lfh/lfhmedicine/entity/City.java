package com.lfh.lfhmedicine.entity;

import lombok.Data;

/**
 * @ProjectName: lfh-hospital
 * @Package: com.lfh.lfhmedicine.entity
 * @ClassName: Nation
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/16 9:24
 * @Version: 1.0
 */
@Data
public class City {
    private Integer id;//主键
    private String name;
    private Integer bid;
}
