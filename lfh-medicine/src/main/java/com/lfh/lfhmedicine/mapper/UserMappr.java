package com.lfh.lfhmedicine.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lfh.lfhmedicine.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @ProjectName: lfh-hospital
 * @Package: com.lfh.lfhmedicine.mapper
 * @ClassName: UserMappr
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/16 9:25
 * @Version: 1.0
 */
@Mapper
@Repository
public interface UserMappr extends BaseMapper<User> {
}
