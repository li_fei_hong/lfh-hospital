package com.lfh.lfhmedicine.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lfh.lfhmedicine.entity.Orders;
import com.lfh.lfhmedicine.entity.ResultEntity;
import com.lfh.lfhmedicine.entity.User;
import com.lfh.lfhmedicine.mapper.UserMappr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ProjectName: lfh-hospital
 * @Package: com.lfh.lfhmedicine.controller
 * @ClassName: UserController
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/16 11:10
 * @Version: 1.0
 */
@RestController
@CrossOrigin
public class UserController {
    @Autowired
    private UserMappr userMappr;
    @RequestMapping("selectUsers")
    public ResultEntity selectUsers(){
        List<User> users = userMappr.selectList(null);
        return ResultEntity.ok(users);
    }

    @GetMapping("getUser")
    public ResultEntity saveOrders(Integer id){
        User user = userMappr.selectById(id);
        return ResultEntity.ok(user);
    }
}
