package com.lfh.lfhmedicine.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lfh.lfhmedicine.entity.City;
import com.lfh.lfhmedicine.entity.Medicine;
import com.lfh.lfhmedicine.entity.ResultEntity;
import com.lfh.lfhmedicine.mapper.CityMapper;
import com.lfh.lfhmedicine.mapper.MedicineMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ProjectName: lfh-hospital
 * @Package: com.lfh.lfhmedicine.controller
 * @ClassName: CityController
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/16 10:18
 * @Version: 1.0
 */
@RestController
@CrossOrigin
public class MedicineController {
    @Autowired
    private MedicineMapper medicineMapper;

    @GetMapping("getYao")
    public ResultEntity selectByBid(Integer id){
        List<Medicine> medicines = medicineMapper.selectList(null);
        return ResultEntity.ok(medicines);
    }
}
