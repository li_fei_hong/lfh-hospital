package com.lfh.lfhmedicine.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lfh.lfhmedicine.entity.Orders;
import com.lfh.lfhmedicine.entity.ResultEntity;
import com.lfh.lfhmedicine.mapper.OrdersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ProjectName: lfh-hospital
 * @Package: com.lfh.lfhmedicine.controller
 * @ClassName: OrdrsController
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/16 14:19
 * @Version: 1.0
 */
@RestController
@CrossOrigin
public class OrdrsController {
    @Autowired
    private OrdersMapper ordersMapper;
    @PostMapping("saveOrders")
    public ResultEntity saveOrders(@RequestBody Orders[] orders){
        for (Orders order : orders) {
            ordersMapper.insert(order);
        }
        return ResultEntity.ok();
    }
    @GetMapping("getOrders")
    public ResultEntity saveOrders(Integer id){
        QueryWrapper<Orders> ordersQueryWrapper = new QueryWrapper<>();
        ordersQueryWrapper.eq("userid",id);
        ordersQueryWrapper.eq("status",0);
        List<Orders> orders = ordersMapper.selectList(ordersQueryWrapper);
        return ResultEntity.ok(orders);
    }
}
