package com.lfh.lfhmedicine.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lfh.lfhmedicine.entity.City;
import com.lfh.lfhmedicine.mapper.CityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ProjectName: lfh-hospital
 * @Package: com.lfh.lfhmedicine.controller
 * @ClassName: CityController
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/16 10:18
 * @Version: 1.0
 */
@RestController
@CrossOrigin
public class CityController {
    @Autowired
    private CityMapper cityMapper;

    @GetMapping("getCity")
    public List<City> selectByBid(Integer id){
        System.out.println(id);
        QueryWrapper<City> cityQueryWrapper = new QueryWrapper<>();
        cityQueryWrapper.eq("bid",id);
        List<City> cities = cityMapper.selectList(cityQueryWrapper);
        return cities;
    }
}
