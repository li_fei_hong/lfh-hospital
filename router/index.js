import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Payment from '@/components/Payment'
import Role from '@/components/Role'
import QRCode from "qrcode"; //引入生成二维码插件
Vue.prototype.QRCode=QRCode;
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/payment',
      name: 'Payment',
      component: Payment
    },{
      path: '/role',
      name: 'Role',
      component: Role
    },

  ]
})
