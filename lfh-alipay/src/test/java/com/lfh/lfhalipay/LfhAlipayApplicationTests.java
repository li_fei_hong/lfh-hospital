package com.lfh.lfhalipay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.lfh.lfhalipay.controller.SelectType;
import com.lfh.lfhalipay.controller.ZhiFuController;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
class LfhAlipayApplicationTests {
    @Autowired
    SelectType selectType;
    @Autowired
    ZhiFuController zhiFuController;
    @Test
    void contextLoads() throws AlipayApiException {
//        String s = zhiFuController.erWeiMa("155555585576");
//        System.out.println(s);
    }
    @Test
    void contextLoads2() throws AlipayApiException {
        AlipayTradeQueryResponse select = selectType.select("1583577141331");
        System.out.println(select.isSuccess());
        System.out.println(select.getBody());
    }

}
