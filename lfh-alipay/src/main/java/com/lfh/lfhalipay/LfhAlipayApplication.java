package com.lfh.lfhalipay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LfhAlipayApplication {

    public static void main(String[] args) {
        SpringApplication.run(LfhAlipayApplication.class, args);
    }

}
