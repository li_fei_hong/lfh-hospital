package com.lfh.lfhalipay.controller;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.CertAlipayRequest;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.lfh.lfhalipay.config.PayConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @ProjectName: lfh-zhifubao
 * @Package: com.lfh.lfhalipay.controller
 * @ClassName: SelectType
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/6 22:28
 * @Version: 1.0
 */
@RestController
public class SelectType {

    @Autowired
    PayConfig payConfig;
    public AlipayTradeQueryResponse select(String order) throws AlipayApiException {
        //        构造client
        CertAlipayRequest certAlipayRequest = new CertAlipayRequest();
        //设置网关地址
        certAlipayRequest.setServerUrl(payConfig.getUrl());
        //设置应用Id
        certAlipayRequest.setAppId(payConfig.getAppid());
        //设置应用私钥
        certAlipayRequest.setPrivateKey(payConfig.getPrivateKey());
        //设置请求格式，固定值json
        certAlipayRequest.setFormat("json");
        //设置字符集
        certAlipayRequest.setCharset("utf-8");
        //设置签名类型
        certAlipayRequest.setSignType("RSA2");
        //设置应用公钥证书路径
        certAlipayRequest.setCertPath(payConfig.getAppCertPath());
        //设置支付宝公钥证书路径
        certAlipayRequest.setAlipayPublicCertPath(payConfig.getAlipayCertPath());
        //设置支付宝根证书路径
        certAlipayRequest.setRootCertPath(payConfig.getAlipayRootCertPath());
        AlipayClient alipayClient = new DefaultAlipayClient(certAlipayRequest); //获得初始化的AlipayClient
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();//创建API对应的request类
        Map<String, String> map = new HashMap<String, String>();
        map.put("out_trade_no", order);
        //......................
//        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
//        request.setBizContent(JSON.toJSONString(map));//订单允许的最晚付款时间
//        //发送请求
//        AlipayTradeQueryResponse response = alipayClient.certificateExecute(request);
        //;;;;;;;;;;;;;;;;;;;;;;;;;;
        request.setBizContent(JSON.toJSONString(map)); //设置业务参数
        AlipayTradeQueryResponse response = alipayClient.certificateExecute(request);//通过alipayClient调用API，获得对应的response类
        return response;
//根据response中的结果继续业务逻辑处理
    }
}
