package com.lfh.lfhalipay.controller;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.CertAlipayRequest;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.lfh.lfhalipay.config.PayConfig;
import com.lfh.lfhalipay.entity.Orders;
import com.lfh.lfhalipay.mapper.OrdersMapper;
import com.lfh.lfhpaystarter.service.AlibabapayQrCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @ProjectName: lfh-zhifubao
 * @Package: com.lfh.controller
 * @ClassName: Controller
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/3 20:12
 * @Version: 1.0
 */
@RestController
@CrossOrigin
public class ZhiFuController {
    @Autowired
    private AlibabapayQrCodeService alibabapayQrCodeService;
    @Autowired
    PayConfig payConfig;
    @Autowired
    private OrdersMapper ordersMapper;
    @PostMapping("erWeiMa")
    public String erWeiMa(@RequestBody Orders orders) throws AlipayApiException {
        System.out.println(orders);
        Map<String, String> map = new HashMap<String, String>();
        map.put("out_trade_no", orders.getOrderid());
        map.put("trade_no", orders.getOrderid());
        map.put("total_amount",orders.getSums().toString());
        map.put("subject", orders.getName());
        map.put("store_id", "NJ_002");
        map.put("timeout_express", "7200m");
//        Orders orders = new Orders();
//        orders.setCreateDate(new Date());
//        orders.setName(map.get("subject"));
//        orders.setOrderId(orderNo);
//        orders.setStatus(0);
//        ordersMapper.insert(orders);//项数据库保存订单
        AlipayTradePrecreateResponse qrCodePayOrder = alibabapayQrCodeService.createQrCodePayOrder(map);
        System.out.println(qrCodePayOrder.getQrCode());
        System.out.println(qrCodePayOrder.getOutTradeNo());
        System.out.println(qrCodePayOrder.getParams());
        return qrCodePayOrder.getQrCode();
////        构造client
//        CertAlipayRequest certAlipayRequest = new CertAlipayRequest();
//        //设置网关地址
//        certAlipayRequest.setServerUrl(payConfig.getUrl());
//        //设置应用Id
//        certAlipayRequest.setAppId(payConfig.getAppid());
//        //设置应用私钥
//        certAlipayRequest.setPrivateKey(payConfig.getPrivateKey());
//        //设置请求格式，固定值json
//        certAlipayRequest.setFormat("json");
//        //设置字符集
//        certAlipayRequest.setCharset("utf-8");
//        //设置签名类型
//        certAlipayRequest.setSignType("RSA2");
//        //设置应用公钥证书路径
//        certAlipayRequest.setCertPath(payConfig.getAppCertPath());
//        //设置支付宝公钥证书路径
//        certAlipayRequest.setAlipayPublicCertPath(payConfig.getAlipayCertPath());
//        //设置支付宝根证书路径
//        certAlipayRequest.setRootCertPath(payConfig.getAlipayRootCertPath());
//        //构造client
//        AlipayClient alipayClient = new DefaultAlipayClient(certAlipayRequest);
//        //构造API请求
////        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
//
//        AlipayTradePrecreateRequest request = new AlipayTradePrecreateRequest();//创建API对应的request类
//
//        request.setNotifyUrl(payConfig.getCallBackUrl());
//        request.setBizContent(JSON.toJSONString(map));//订单允许的最晚付款时间
//
//        //发送请求
//        AlipayTradePrecreateResponse alipayTradePrecreateResponse = alipayClient.certificateExecute(request);
//        System.out.println(alipayTradePrecreateResponse.getQrCode());
//     return  alipayTradePrecreateResponse.getQrCode();
    }
}
