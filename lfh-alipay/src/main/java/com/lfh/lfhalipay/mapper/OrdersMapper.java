package com.lfh.lfhalipay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lfh.lfhalipay.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @ProjectName: lfh-zhifubao
 * @Package: com.lfh.lfhalipay.mapper
 * @ClassName: OrdersMapper
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/6 19:25
 * @Version: 1.0
 */
@Mapper
@Repository
public interface OrdersMapper extends BaseMapper<Orders> {

}
