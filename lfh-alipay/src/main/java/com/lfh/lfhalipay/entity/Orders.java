package com.lfh.lfhalipay.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @ProjectName: lfh-zhifubao
 * @Package: com.lfh.lfhalipay.entity
 * @ClassName: Orders
 * @Author: lfh
 * @Description:
 * @Date: 2020/3/6 18:53
 * @Version: 1.0
 */
@Data
@TableName("orders")
public class Orders {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;//主键
    private String orderid;//订单编号
    private String name;//商品名称
    private Integer status;//是否支付
    private String tradeno;//支付宝订单号
    private String buyerid;//买家支付宝用户号
    private Double sums;//支付总金额
}
